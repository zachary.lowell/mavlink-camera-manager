/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <string.h>  

#include <modal_pipe.h>
#include <modal_journal.h>
#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <voxl_cutils.h>
#include <turbojpeg.h>
#include <jpeglib.h>
#include <time.h>

#include <c_library_v2/common/mavlink.h>

#include "qgc.h"
// #include "image_context.h"

#define MAX_MESSAGE_LENGTH 256
#define UDP_READ_BUF_LEN 512
#define FMT_INVALID 0
#define FMT_YUV 2
#define FMT_NV 3

// TODO: Should the system ID be configurable?
static int mavlink_system_id = 1;

// Pointer to the application data
static context_data *context = NULL;

// Socket definitions
static int qgc_sockfd;
static struct sockaddr_in qgc_servaddr;

// Thread definitions
static pthread_t qgc_heartbeat_thread_id;
static pthread_t qgc_thread_id;

// Mavlink message definitions
static unsigned char ping_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int ping_message_size = 0;
static unsigned char camera_heartbeat_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_heartbeat_message_size = 0;
static unsigned char command_ack_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int command_ack_message_size = 0;
static unsigned char camera_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_information_message_size = 0;
static unsigned char camera_settings_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_settings_message_size = 0;
static unsigned char video_stream_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int video_stream_information_message_size = 0;
static unsigned char video_stream_status_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int video_stream_status_message_size = 0;
static unsigned char storage_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int storage_information_message_size = 0;
static unsigned char camera_capture_status_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_capture_status_message_size = 0;
static unsigned char camera_image_captured_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_image_captured_message_size = 0;
static unsigned char command_long_buffer[MAX_MESSAGE_LENGTH];
static unsigned int command_long_message_size = 0;


// Converts a nv12 image to YUV420
static void _cvt_nv_yuv(unsigned char* frame, int width, int height, int isNV12) {
	int y_size = width * height;

	unsigned char* uv_tmp = (unsigned char*)malloc(y_size / 2);
	memcpy(uv_tmp, frame + y_size, (y_size / 2));

	unsigned char* u = frame + y_size;
	unsigned char* v = frame + (y_size * 5 / 4);

	for (int i = 0; i < y_size / 4; i++) {
		if (isNV12) {  // nv12
			u[i] = uv_tmp[(i * 2)];
			v[i] = uv_tmp[(i * 2) + 1];
		} else {  // nv21
			u[i] = uv_tmp[(i * 2) + 1];
			v[i] = uv_tmp[(i * 2)];
		}
	}
	free(uv_tmp);
}

static int tj_fmt_from_mpa(camera_image_metadata_t meta, int* outfmt) {
	switch (meta.format) {
		case IMAGE_FORMAT_YUV420:
			*outfmt = TJSAMP_420;
			return FMT_YUV;

		case IMAGE_FORMAT_YUV422:
			*outfmt = TJSAMP_422;
			return FMT_YUV;

		case IMAGE_FORMAT_NV21:
		case IMAGE_FORMAT_NV12:
			*outfmt = TJSAMP_420;
			return FMT_NV;

		case IMAGE_FORMAT_STEREO_NV12:
		case IMAGE_FORMAT_STEREO_NV21:
			*outfmt = TJSAMP_420;
			return FMT_NV;

		default:
			return FMT_INVALID;
	}
}

static void _cam_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Camera server Connected\n");
}

// called whenever we disconnect from the server
static void _cam_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Camera server Disconnected\n");
}

// Given a file path, create all constituent directories if missing
static void CreateParentDirs(const char *file_path)
{
  char *dir_path = (char *) malloc(strlen(file_path) + 1);
  const char *next_sep = strchr(file_path, '/');
  while (next_sep != NULL) {
    int dir_path_len = next_sep - file_path;
    memcpy(dir_path, file_path, dir_path_len);
    dir_path[dir_path_len] = '\0';
    mkdir(dir_path, S_IRWXU|S_IRWXG|S_IROTH);
    next_sep = strchr(next_sep + 1, '/');
  }
  free(dir_path);
}

// camera helper callback whenever a frame arrives
static void _cam_helper_cb(
    __attribute__((unused))int ch,
                           camera_image_metadata_t meta,
                           char* frame)
{
	if (frame == NULL) return -1;
    
    char path[14] = "/data/images/";
    char frame_str[43] = "/data/images/snapshot_modalai_hires_frame ";
    char extension[5] = ".jpg";

    CreateParentDirs(path);

    char converted_frame_counter[100];
    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    sprintf (converted_frame_counter ,"%s", asctime (timeinfo) );
    converted_frame_counter[strcspn(converted_frame_counter, "$\n")] = 0;
    strcat(converted_frame_counter, extension);

    strcat(frame_str, converted_frame_counter);

	int ret = 0;
	/* BEGIN JPEG COMPRESS */
	tjhandle tjInstance = NULL;
	unsigned long jpegSize = 0;
	unsigned char* jpegBuf = NULL; /* Dynamically allocate the JPEG buffer */
	int outSubsamp;
	int outQual = 100;
	int flags = 0;
	// speed up flags
	flags |= TJFLAG_FASTUPSAMPLE;
	flags |= TJFLAG_FASTDCT;

	int width = meta.width;
	int height = meta.height;

	switch (tj_fmt_from_mpa(meta, &outSubsamp)) {
		case FMT_NV:

			_cvt_nv_yuv((unsigned char*)frame, width, height, meta.format == IMAGE_FORMAT_NV12);
			// intentionally fall through this case!

		case FMT_YUV:

			if ((tjInstance = tjInitCompress()) == NULL)
				printf("Initializing compressor");

			if (tjCompressFromYUV(tjInstance, (uint8_t*)frame, width, 1, height, outSubsamp,
								&jpegBuf, &jpegSize, outQual, flags) < 0)
				printf("Compressing Image");

			tjDestroy(tjInstance);
			tjInstance = NULL;
			/* END JPEG COMPRESS */
			break;

		case FMT_INVALID:
		default:
			ret = -1;
			break;
	}


	if (jpegBuf == NULL) ret = -1;

	// dont even try to save the image if we had issues
	if (ret == 0){
		FILE* file = fopen(frame_str, "wb");
		if (!file) {
			ret = -2;
		}
		unsigned int n_written = fwrite(jpegBuf, 1, jpegSize, file);
		if(n_written!=jpegSize) ret = -3;
		fclose(file);
	}

	tjFree(jpegBuf);

    pipe_client_close_all();
}

static void _video_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Video server Connected\n");
}

// called whenever we disconnect from the server
static void _video_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    M_PRINT("Video server Disconnected\n");
}

static void _video_save_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame)
{
    
	if(meta.format != IMAGE_FORMAT_H264){
		fprintf(stderr, "error, can only record pipes with type h264\n");
        pipe_client_close_all();
	}
    if(! context->encoded_file){
        // wb: If the file exists, its contents are cleared
        context->encoded_file = fopen(context->video_name, "wb");
        if(! context->encoded_file){
            printf("Cannot open video file!!!! \n");
            pipe_client_close_all();
        } else {
            printf("Opened output file: %s\n", context->video_name);
        }
    }   

	int ret = fwrite(frame, meta.size_bytes, 1, context->encoded_file);

	if(ret!=1){
		perror("failed to write to disk");
        pipe_client_close_all();
	}
    return;

}

static uint32_t get_ms_since_boot() {
    struct timespec tp;
    int rc = clock_gettime(CLOCK_MONOTONIC, &tp);
    uint32_t ms_since_boot = ((tp.tv_sec * 1000) + (tp.tv_nsec / 1000000));
    if (context->debug) printf("Milliseconds since boot %u ms\n", ms_since_boot);
    return ms_since_boot;
}

static void send_camera_settings() {
    mavlink_message_t send_message;

    if (context->debug) printf("Sending camera settings\n");

    // Send the camera settings
    /* @param time_boot_ms [ms] Timestamp (time since system boot).
     * @param mode_id  Camera mode
     * @param zoomLevel  Current zoom level (0.0 to 100.0, NaN if not known)
     * @param focusLevel  Current focus level (0.0 to 100.0, NaN if not known)
     */
    printf("VALUE OF CAMERA MODE: %i \n", context->camera_mode);

    mavlink_msg_camera_settings_pack(mavlink_system_id, context->camera_component_id,
                                     &send_message,
                                     get_ms_since_boot(),
                                     context->camera_mode,
                                     NAN,
                                     NAN);
    camera_settings_message_size = mavlink_msg_to_send_buffer(camera_settings_send_buffer,
                                                              (const mavlink_message_t *) &send_message);
    sendto(qgc_sockfd, camera_settings_send_buffer, \
           camera_settings_message_size, MSG_CONFIRM, \
           (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
}

static void send_mavlink_command_ack(mavlink_message_t* msg, uint16_t mav_cmd) {
    mavlink_message_t send_message;

    if (context->debug) printf("Sending command ACK\n");

    mavlink_msg_command_ack_pack(
                                 mavlink_system_id,
                                 context->camera_component_id,
                                 &send_message,
                                 mav_cmd,
                                 MAV_RESULT_ACCEPTED,
                                 0,
                                 0,
                                 msg->sysid,
                                 msg->compid);
    command_ack_message_size = mavlink_msg_to_send_buffer(command_ack_send_buffer,
                                                          (const mavlink_message_t *) &send_message);
    sendto(qgc_sockfd, command_ack_send_buffer, \
           command_ack_message_size, MSG_CONFIRM, \
           (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
}

static void qgc_message_handler(mavlink_message_t* msg) {
    mavlink_message_t send_message;

    if (context->debug) printf("Received msg: ID: %d sys:%d comp:%d\n", msg->msgid, msg->sysid, msg->compid);
    //printf("value of msgid: %i\n", msg->msgid);
    switch (msg->msgid)
    {
    case MAVLINK_MSG_ID_HEARTBEAT:
        if (context->debug) printf("Heartbeat message at camera\n");
        break;
    case MAVLINK_MSG_ID_PING:
        {
            mavlink_ping_t *ping_data = (mavlink_ping_t*) msg->payload64;
            if ((ping_data->target_system == 0) && (ping_data->target_component == 0)) {
                printf("Got a Ping Request from system %d, component %d\n", msg->sysid, msg->compid);
                mavlink_msg_ping_pack(mavlink_system_id, context->camera_component_id,
                                      &send_message,
                                      ping_data->time_usec,
                                      ping_data->seq,
                                      msg->sysid,
                                      msg->compid);
                ping_message_size = mavlink_msg_to_send_buffer(ping_send_buffer,
                                                               (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, ping_send_buffer, \
                       ping_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            } else if (context->debug) {
                printf("Got a Ping Response\n");
                printf("\ttime_usec: %" PRIu64 ", seq: %u, target_system: %d, target_component: %d\n",
                       ping_data->time_usec, ping_data->seq, ping_data->target_system,
                       ping_data->target_component);
            }
        }
        break;

    case MAVLINK_MSG_ID_COMMAND_LONG:
        {
            uint8_t componentID;
            mavlink_command_long_t cmd;
            mavlink_msg_command_long_decode(msg, &cmd);
            mavlink_command_long_t digicam_ctrl_cmd;
            
            if (context->debug) printf("Command long message at camera\n");

            mavlink_command_long_t *command_long_data =
                                    (mavlink_command_long_t*) msg->payload64;
            //printf("Value of command long: %i\n", command_long_data->command);
            if (command_long_data->target_system != mavlink_system_id) {
                fprintf(stderr, "WARNING: Command system ID is not a match\n");
                break;
            }
            if (command_long_data->target_component != context->camera_component_id) {
                fprintf(stderr, "WARNING: Command component ID is not a match\n");
                break;
            }


            if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_INFORMATION) 
            {
                if (context->debug) {
                    printf("Got camera information request\n");


                    if (((int) command_long_data->param1) != 1) {
                        printf("...not requesting capabilities???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_CAMERA_INFORMATION);

                // Send the camera information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param vendor_name  Name of the camera vendor
                 * @param model_name  Name of the camera model
                 * @param firmware_version  Version of the camera firmware (v << 24 & 0xff = Dev, v << 16 & 0xff = Patch, v << 8 & 0xff = Minor, v & 0xff = Major)
                 * @param focal_length [mm] Focal length
                 * @param sensor_size_h [mm] Image sensor size horizontal
                 * @param sensor_size_v [mm] Image sensor size vertical
                 * @param resolution_h [pix] Horizontal image resolution
                 * @param resolution_v [pix] Vertical image resolution
                 * @param lens_id  Reserved for a lens ID
                 * @param flags  Bitmap of camera capability flags.
                 * @param cam_definition_version  Camera definition version (iteration)
                 * @param cam_definition_uri  Camera definition URI (if any, otherwise only basic functions will be available).
                 *                            HTTP- (http://) and MAVLink FTP- (mavlinkftp://) formatted URIs are allowed (and
                 *                            both must be supported by any GCS that implements the Camera Protocol).
                 */


                uint32_t camera_capabilities = CAMERA_CAP_FLAGS_CAPTURE_IMAGE | CAMERA_CAP_FLAGS_CAPTURE_VIDEO |
                        CAMERA_CAP_FLAGS_HAS_MODES | CAMERA_CAP_FLAGS_HAS_BASIC_ZOOM |
                        CAMERA_CAP_FLAGS_HAS_VIDEO_STREAM;
                            mavlink_msg_camera_information_pack(mavlink_system_id, 
                                                                context->camera_component_id,
                                                                &send_message,
                                                                get_ms_since_boot(),
                                                                (const uint8_t *) "Unknown",
                                                                (const uint8_t *) context->camera_name,
                                                                1 | (218 << 8),
                                                                100.0,
                                                                10.0,
                                                                10.0,
                                                                640,
                                                                480,
                                                                0,
                                                                camera_capabilities,
                                                                0,
                                                                NULL);

                camera_information_message_size = mavlink_msg_to_send_buffer(camera_information_send_buffer,
                                                                             (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_information_send_buffer, \
                       camera_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } 

            else if (command_long_data->command == MAV_CMD_SET_CAMERA_MODE) 
            {
                if (context->debug) 
                {
                    printf("Got camera set mode\n");
                }
                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_SET_CAMERA_MODE);

                if(context->camera_mode == CAMERA_MODE_VIDEO){
                    context->camera_mode = CAMERA_MODE_IMAGE;
                } else {
                    context->camera_mode = CAMERA_MODE_VIDEO;
                }
                send_camera_settings();
            } 

            else if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_SETTINGS) 
            {
                if (context->debug) 
                {
                    printf("Got camera settings request\n");

                    if (((int) command_long_data->param1) != 1) 
                    {
                        printf("...not requesting settings???...\n");
                    }
                }
                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_CAMERA_SETTINGS);

                // Send the camera settings
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param mode_id  Camera mode
                 * @param zoomLevel  Current zoom level (0.0 to 100.0, NaN if not known)
                 * @param focusLevel  Current focus level (0.0 to 100.0, NaN if not known)
                 */
                
                mavlink_msg_camera_settings_pack(mavlink_system_id, 
                                                 context->camera_component_id,
                                                 &send_message,
                                                 get_ms_since_boot(),
                                                 context->camera_mode,
                                                 NAN,
                                                 NAN);
                camera_settings_message_size = mavlink_msg_to_send_buffer(camera_settings_send_buffer,
                                                                          (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_settings_send_buffer, \
                       camera_settings_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } 
            
            else if (command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION) 
            {
                if (context->debug) 
                {
                    printf("Got video stream information request for stream: %d\n",
                           (int) command_long_data->param1);
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION);

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param count  Number of streams available.
                 * @param type  Type of stream.
                 * @param flags  Bitmap of stream status flags.
                 * @param framerate [Hz] Frame rate.
                 * @param resolution_h [pix] Horizontal resolution.
                 * @param resolution_v [pix] Vertical resolution.
                 * @param bitrate [bits/s] Bit rate.
                 * @param rotation [deg] Video image rotation clockwise.
                 * @param hfov [deg] Horizontal Field of view.
                 * @param name  Stream name.
                 * @param uri  Video stream URI (TCP or RTSP URI ground station should connect to) or port number (UDP port ground station should listen to).
                 */
                mavlink_msg_video_stream_information_pack(mavlink_system_id, 
                                                          context->camera_component_id,
                                                          &send_message,
                                                          0,
                                                          0,
                                                          VIDEO_STREAM_TYPE_RTSP,
                                                          VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                          15.0,
                                                          640,
                                                          480,
                                                          1000000,
                                                          0,  // QGC does NOT rotate the image
                                                          60,
                                                          "cam-stream",
                                                          context->rtsp_server_uri);
                video_stream_information_message_size = mavlink_msg_to_send_buffer(video_stream_information_send_buffer,
                                                                                   (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, video_stream_information_send_buffer, \
                       video_stream_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } 
            
            else if (command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_STATUS) 
            {
                if (context->debug) 
                {
                    printf("Got video stream status request for stream: %d\n",
                           (int) command_long_data->param1);
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_VIDEO_STREAM_STATUS);

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param flags  Bitmap of stream status flags
                 * @param framerate [Hz] Frame rate
                 * @param resolution_h [pix] Horizontal resolution
                 * @param resolution_v [pix] Vertical resolution
                 * @param bitrate [bits/s] Bit rate
                 * @param rotation [deg] Video image rotation clockwise
                 * @param hfov [deg] Horizontal Field of view
                 */
                mavlink_msg_video_stream_status_pack(mavlink_system_id, context->camera_component_id,
                                                     &send_message,
                                                     0,
                                                     VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                     15.0,
                                                     640,
                                                     480,
                                                     1000000,
                                                     0,  // QGC does NOT rotate the image
                                                     60);
                video_stream_status_message_size = mavlink_msg_to_send_buffer(video_stream_status_send_buffer,
                                                                              (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, video_stream_status_send_buffer, \
                      video_stream_status_message_size, MSG_CONFIRM, \
                      (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } 
            
            else if (command_long_data->command == MAV_CMD_REQUEST_STORAGE_INFORMATION) 
            {
                
                if (context->debug) 
                {
                    printf("Got storage information request. Storage ID: %d\n",
                           (int) command_long_data->param1);

                    if (((int) command_long_data->param2) != 1) 
                    {
                        printf("...not requesting information???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_STORAGE_INFORMATION);

                // new extension field to the storage_information packet we don't use
                char name[32];
                memset(name,0,32);

                // Send the storage information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param storage_id  Storage ID (1 for first, 2 for second, etc.)
                 * @param storage_count  Number of storage devices
                 * @param status  Status of storage
                 * @param total_capacity [MiB] Total capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param used_capacity [MiB] Used capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param available_capacity [MiB] Available storage capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param read_speed [MiB/s] Read speed.
                 * @param write_speed [MiB/s] Write speed.
                 */
                mavlink_msg_storage_information_pack(mavlink_system_id, context->camera_component_id,
                                                     &send_message,
                                                     get_ms_since_boot(),
                                                     1,
                                                     1,
                                                     3,   // STORAGE_STATUS_NOT_SUPPORTED
                                                     0.0,
                                                     100.0,
                                                     0.0,
                                                     0.0,
                                                     0.0,
                                                     0,
                                                     name,
                                                     0);
                storage_information_message_size = mavlink_msg_to_send_buffer(storage_information_send_buffer,
                                                                              (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, storage_information_send_buffer, \
                       storage_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            } 
            
            else if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS) 
            {
                if (context->debug) {
                    printf("Got camera capture status request\n");

                    if (((int) command_long_data->param1) != 1) {
                        printf("...not requesting status???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg, MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS);

                // Send the camera capture status
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param image_status  Current status of image capturing (0: idle, 1: capture in progress, 2: interval set but idle, 3: interval set and capture in progress)
                 * @param video_status  Current status of video capturing (0: idle, 1: capture in progress)
                 * @param image_interval [s] Image capture interval
                 * @param recording_time_ms [ms] Time since recording started
                 * @param available_capacity [MiB] Available storage capacity.


                 * @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param image_status  Current status of image capturing (0: idle, 1: capture in progress, 2: interval set but idle, 3: interval set and capture in progress
                 * @param video_status  Current status of video capturing (0: idle, 1: capture in progress)
                 * @param image_interval [s] Image capture interval
                 * @param recording_time_ms [ms] Time since recording started
                 * @param available_capacity [MiB] Available storage capacity.
                 * @param image_count  Total number of images captured ('forever', or until reset using MAV_CMD_STORAGE_FORMAT).


                 */
                if(context->camera_mode == CAMERA_MODE_IMAGE){
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context->camera_component_id,
                                                        &send_message,
                                                        get_ms_since_boot(),
                                                        0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0);
                } else if (context->camera_mode == CAMERA_MODE_VIDEO && context->started_recording == 1){
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context->camera_component_id,
                                                        &send_message,
                                                        get_ms_since_boot(),
                                                        0,
                                                        1,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0);
                } else {
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context->camera_component_id,
                                                        &send_message,
                                                        get_ms_since_boot(),
                                                        0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0); 
                }
                camera_capture_status_message_size = mavlink_msg_to_send_buffer(camera_capture_status_send_buffer,
                                                                                (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_capture_status_send_buffer, \
                    camera_capture_status_message_size, MSG_CONFIRM, \
                    (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            }

            else if (command_long_data->command == MAV_CMD_IMAGE_START_CAPTURE) 
            {
                pipe_client_set_connect_cb(0, _cam_connect_cb, NULL);
                pipe_client_set_disconnect_cb(0, _cam_disconnect_cb, NULL);
                pipe_client_set_camera_helper_cb(0, _cam_helper_cb, NULL);
                pipe_client_open(0, "hires", "voxl-mavlink-camera-server", EN_PIPE_CLIENT_CAMERA_HELPER, 0);

                if (context->debug) 
                {
                    printf("Calling start capture");
                }

                send_mavlink_command_ack(msg, MAV_CMD_IMAGE_START_CAPTURE);

                // Set the flag to initiate a video frame grab and JPEG encode
                context->grab_image = 1;

                // Send back CAMERA_IMAGE_CAPTURED
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param time_utc [us] Timestamp (time since UNIX epoch) in UTC. 0 for unknown.
                 * @param camera_id  Deprecated/unused. Component IDs are used to differentiate multiple cameras.
                 * @param lat [degE7] Latitude where image was taken
                 * @param lon [degE7] Longitude where capture was taken
                 * @param alt [mm] Altitude (MSL) where image was taken
                 * @param relative_alt [mm] Altitude above ground
                 * @param q  Quaternion of camera orientation (w, x, y, z order, zero-rotation is 1, 0, 0, 0)
                 * @param image_index  Zero based index of this image (i.e. a new image will have index CAMERA_CAPTURE_STATUS.image count -1)
                 * @param capture_result  Boolean indicating success (1) or failure (0) while capturing this image.
                 * @param file_url  URL of image taken. Either local storage or http://foo.jpg if camera provides an HTTP interface.
                 * @return length of the message in bytes (excluding serial stream start sign)
                 */
                 
                float q[4] = {1.0, 0.0, 0.0, 0.0};
                char fname[MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED_LEN] = "modalai-capture.jpg";
                mavlink_msg_camera_image_captured_pack(mavlink_system_id,
                                                       context->camera_component_id,
                                                       &send_message,
                                                       get_ms_since_boot(),
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       q,
                                                       0,
                                                       1,
                                                       fname);
                camera_image_captured_message_size = mavlink_msg_to_send_buffer(camera_image_captured_buffer,
                                                                (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_image_captured_buffer, \
                       camera_image_captured_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            
            } else if (command_long_data->command == MAV_CMD_IMAGE_STOP_CAPTURE) 
            {

                if (context->debug) 
                {
                    printf("Calling stop capture");
                }
                send_mavlink_command_ack(msg, MAV_CMD_IMAGE_STOP_CAPTURE);

            } else if (command_long_data->command == MAV_CMD_VIDEO_START_CAPTURE) 
            {

                if (context->debug) 
                {
                    printf("Started a video!\n");
                }
                context->started_recording = 1;
                pipe_client_close_all();

                send_mavlink_command_ack(msg, MAV_CMD_VIDEO_START_CAPTURE);

                char vid_path[14] = "/data/video/";
                CreateParentDirs(vid_path);

                char frame_str[43] = "/data/video/h264_modalai_hires_stream ";
                char extension[5] = ".h264";

                char converted_frame_counter[100];
                time_t rawtime;
                struct tm * timeinfo1;
                time ( &rawtime );
                timeinfo1 = localtime ( &rawtime );
                sprintf (converted_frame_counter ,"%s", asctime (timeinfo1) );
                converted_frame_counter[strcspn(converted_frame_counter, "$\n")] = 0;
                strcat(converted_frame_counter, extension);

                strcat(frame_str, converted_frame_counter);
                context->video_name = frame_str;
                FILE * encoded_out;
                context->encoded_file = encoded_out;

                pipe_client_set_connect_cb(1, _video_connect_cb, context);
                pipe_client_set_disconnect_cb(1, _video_disconnect_cb, context);
                pipe_client_set_camera_helper_cb(1, _video_save_cb, context);
                pipe_client_open(1, "hires_stream", "voxl-mavlink-camera-video-save", EN_PIPE_CLIENT_CAMERA_HELPER, 0);

            } else if (command_long_data->command == MAV_CMD_VIDEO_STOP_CAPTURE) 
            {
                if (context->debug) 
                {
                    printf("Stopped a video!");
                }

                context->started_recording = 0;
                send_mavlink_command_ack(msg, MAV_CMD_VIDEO_STOP_CAPTURE);

                pipe_client_close_all();
                fclose(context->encoded_file);
             }
            
            else 
            {
                if (context->debug) printf("Got unknown command %d\n\n\n", command_long_data->command);
                break;
            }
            if (context->debug) printf("Confirmation number: %d\n", command_long_data->confirmation);
        }

        break;

    default:
        if (context->debug) printf("Unknown message type : %d\n\n\n\n", msg->msgid);
        break;
    }
}

static void* qgc_heartbeat_thread_function(__attribute__((unused)) void *vargp) {

    unsigned int addr = qgc_servaddr.sin_addr.s_addr;

    if (context->debug) {
        printf("QGC Address: %d.%d.%d.%d:%d\n", (addr & 0xFF), (addr & 0xFF00) >> 8,
               (addr & 0xFF0000) >> 16, (addr & 0xFF000000) >> 24, ntohs(qgc_servaddr.sin_port));
    }

    while (context->running) {
        if (context->debug) printf("Sending heartbeat from camera\n");

        sendto(qgc_sockfd, camera_heartbeat_send_buffer, \
               camera_heartbeat_message_size, MSG_CONFIRM, \
               (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

        // Send heartbeats at 1 Hz rate as per Mavlink protocol
        sleep(1);
    }

    return NULL;
}

static void* qgc_thread_function(__attribute__((unused)) void *vargp) {
    struct sockaddr_in qgc_recvaddr;
    int bytes_read = 0;
    socklen_t addrlen = sizeof(qgc_recvaddr);
    unsigned char write_data_buffer[UDP_READ_BUF_LEN];
    mavlink_message_t msg;
    mavlink_status_t status;
    unsigned int rx_drop_count = 0;
    int msg_received = 0;

    memset(&qgc_recvaddr, 0, sizeof(qgc_recvaddr));

    while (context->running) {
        // Receive UDP message from ground control
        bytes_read = recvfrom(qgc_sockfd, (char*) write_data_buffer,
                              UDP_READ_BUF_LEN, MSG_WAITALL,
                              (struct sockaddr*) &qgc_recvaddr, &addrlen);

        if (bytes_read > 0) {
            if (context->debug) printf("read %d bytes from QGC UDP port\n", bytes_read);

            int i = 0;
            // do the mavlink byte-by-byte parsing
            for (; i < bytes_read; i++) {
                msg_received = mavlink_parse_char(0, write_data_buffer[i], &msg, &status);

                // check for dropped packets
                if (status.packet_rx_drop_count != rx_drop_count) {
                    fprintf(stderr,"DROPPED %d PACKETS\n", status.packet_rx_drop_count);
                }
                rx_drop_count = status.packet_rx_drop_count;

                // Send valid messages to the message handler
                if (msg_received) qgc_message_handler(&msg);
            }
            if (context->debug) {
                if ((i == bytes_read) && ( ! msg_received)) {
                    printf("Looked at all bytes and no valid message\n");
                }
            }
        }
    }

    printf("exiting qgc_thread_function\n");
    return NULL;
}

int qgc_init(context_data *ctx) {
    mavlink_message_t send_message;

    if (ctx == NULL) {
        fprintf(stderr, "ERROR: Null context pointer in qgc_init\n");
        return -1;
    }

    context = ctx;

    if (context->camera_id < MAX_MAVLINK_CAMERAS) {
        context->camera_component_id = MAV_COMP_ID_CAMERA + context->camera_id;
    } else {
        fprintf(stderr, "ERROR: Camera ID must be between 0 and 5\n");
        return PROGRAM_ERROR;
    }

    // Creating socket file descriptor for ground control connection
    if ((qgc_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        fprintf(stderr, "ERROR: QGC socket creation failed\n");
        return PROGRAM_ERROR;
    }

    // Now set up our QGC endpoint address so that we can send messages
    memset(&qgc_servaddr, 0, sizeof(qgc_servaddr));
    qgc_servaddr.sin_family = AF_INET; // IPv4
    qgc_servaddr.sin_addr.s_addr = inet_addr(context->qgc_ip_address);
    qgc_servaddr.sin_port = htons(14550);

    printf("component_id: %i", context->camera_component_id);

    // Pack up the heartbeat message. It never changes.
    mavlink_msg_heartbeat_pack(
                                mavlink_system_id, 
                                context->camera_component_id,
                                &send_message, 
                                MAV_TYPE_GENERIC,
                                MAV_AUTOPILOT_GENERIC, 
                                0, 
                                0, 
                                0);
    camera_heartbeat_message_size = mavlink_msg_to_send_buffer(camera_heartbeat_send_buffer,
                                                              (const mavlink_message_t *) &send_message);

    // Start the thread to receive and process incoming GCS messages.
    pthread_create(&qgc_thread_id, NULL, qgc_thread_function, NULL);

    // Start the thread to send out the periodic heartbeat message.
    pthread_create(&qgc_heartbeat_thread_id, NULL, qgc_heartbeat_thread_function, NULL);

    return 0;
}

void qgc_shutdown() {
    if (context) {
        if (context->debug) printf("Waiting for the threads to exit\n");
        pthread_join(qgc_heartbeat_thread_id, NULL);
        pthread_join(qgc_thread_id, NULL);
    }
}