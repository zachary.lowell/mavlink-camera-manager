/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "context.h"
#include "qgc.h"

// This is the main data structure for the application. It is passed / shared
// with other modules as needed.
static context_data context;

// Used to capture ctrl-c signal to allow graceful exit
void int_handler(int dummy) {
    printf("Got SIGINT, exiting\n");
    context.running = 0;
}

static void print_help() {
    printf("Usage: mavlink-camera-manager <options>\n");
    printf("Options:\n");
    printf("-h     print this help message\n");
    printf("-d     Enable debug messages\n");
    printf("-g     GCS IP address\n");
    printf("-r     RTSP URI\n");
    printf("-i     camera id (default is 0)\n");
    printf("-n     camera name (default is \"Camera X\" where X is camera id)\n");
    printf("\n");
}

int main(int argc, char *argv[]) {
    int extra_options = 0;
    int command_line_option = 0;

    while ((command_line_option = getopt(argc, argv, ":hdg:r:i:n:")) != -1) {
        switch (command_line_option) {
        case 'h':
            print_help();
            return 0;
        case 'd':
            context.debug = 1;
            break;
        case 'g':
            printf("Using QGC IP address: %s\n", optarg);
            strncpy(context.qgc_ip_address, optarg, QGC_IP_ADDRESS_MAX_SIZE);
            break;
        case 'r':
            printf("RTSP server URL: %s\n", optarg);
            strncpy(context.rtsp_server_uri, optarg, RTSP_URI_MAX_SIZE);
            break;
        case 'n':
            printf("Camera name: %s\n", optarg);
            strncpy(context.camera_name, optarg, CAMERA_NAME_MAX_SIZE);
            break;
        case 'i':
            context.camera_id = atoi(optarg);
            printf("Using camera id: %d\n", context.camera_id);
            break;
        case ':':
            fprintf(stderr, "Error: Option needs a value\n");
            print_help();
            return 0;
        case '?':
            printf("unknown option: %c\n", optopt);
            print_help();
            return 0;
        }
    }

    for (; optind < argc; optind++) {
        fprintf(stderr, "Error: Extra argument: %s\n", argv[optind]);
        extra_options = 1;
    }
    if (extra_options) {
        print_help();
        return PROGRAM_ERROR;
    }

    // Some sanity checking on the command line options
    int qgc_ip_address_length = strlen(context.qgc_ip_address);
    if ( ! qgc_ip_address_length) {
        fprintf(stderr, "Error, Must supply a QGC IP address. Set with the -g option\n");
        print_help();
        return PROGRAM_ERROR;
    }
    if ((qgc_ip_address_length < 7) || (qgc_ip_address_length > 15)) {
        fprintf(stderr, "Error, Invalid QGC IP address %s. Set with the -g option\n",
                context.qgc_ip_address);
        print_help();
        return PROGRAM_ERROR;
    }
    int rtsp_uri_length = strlen(context.rtsp_server_uri);
    if ( ! rtsp_uri_length) {
        fprintf(stderr, "Error, Must supply an RTSP URI. Set with the -r option\n");
        print_help();
        return PROGRAM_ERROR;
    }

    // Setup default camera name if one wasn't provided
    int camera_name_length = strlen(context.camera_name);
    if ( ! camera_name_length) {
        //                           01234567
        strcpy(context.camera_name, "Camera X");
        context.camera_name[7] = '0' + context.camera_id;
    }

    if (context.debug) {
        printf("Parameter summary:\n");
        printf("\tQGC IP address: %s\n", context.qgc_ip_address);
        printf("\tRTSP URI: %s\n", context.rtsp_server_uri);
        printf("\tCamera ID: %d\n", context.camera_id);
        printf("\tCamera name: %s\n", context.camera_name);
    }

    context.running = 1;

    // Setup our signal handler to catch ctrl-c
    signal(SIGINT, int_handler);

    if (qgc_init(&context) == PROGRAM_ERROR) {
        fprintf(stderr, "QGC init failed\n");
        return PROGRAM_ERROR;
    } else {
        if (context.debug) printf("QGC interface started\n");
    }

    // Idle loop. The real work is done in the QGC module threads.
    while (context.running) sleep(1);

    (void) qgc_shutdown();

    return 0;
}
